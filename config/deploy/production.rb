server '52.67.173.35', user: 'ec2-user', roles: %w{app db web}
set :ssh_options, { forward_agent: true, keys: ["#{ENV['HOME']}/Downloads/agilez.pem"] }